const { AsyncRouter } = require('express-async-router');

class Issues {

    constructor(repository, errors) {
        this._repository = repository;
        this._errors = errors;
        this._statuses = ['open', 'pending', 'closed'];
    }

    async getIssues() {
        return this._repository.getIssues();
    }

    async updateIssue(req) {
        const body = req.body;

        if (!('id' in body && 'status' in body)) {
            throw new this._errors.ValidationError({
                message: 'Can not find \'id\' and/or \'status\' in payload'
            });
        }

        const issue = await this._repository.getIssueById(body.id);

        if (!issue) {
            throw new this._errors.NotFoundError();
        }

        const differenceOfIndexes = this._statuses.indexOf(issue.status) - this._statuses.indexOf(body.status);
        if (!this._statuses.includes(body.status) || differenceOfIndexes !== -1) {
            throw new this._errors.ValidationError({
                message: 'You can only set status to next value (open, pending, closed)'
            });
        }

        return this._repository.updateStatus(body);
    }

    async createIssues(req) {
        if (!('title' in req.body && 'description' in req.body)) {
            throw new this._errors.ValidationError({
                message: 'Can not find \'title\' and/or \'description\' in payload'
            });
        }

        return this._repository.createIssue(req.body);
    }

}

module.exports = (repository, errors) => {
    const router = AsyncRouter();
    const issues = new Issues(repository, errors);

    router.get('/issues', issues.getIssues.bind(issues));
    router.put('/issues', issues.updateIssue.bind(issues));
    router.post('/issues', issues.createIssues.bind(issues));

    return router;
};