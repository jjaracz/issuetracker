const express = require('express');
const bodyParser = require('body-parser');
const issuesEndpoint = require('./endpoints/issues');
const IssueRepository = require('./repositories/issues');
const DbService = require('./services/db');
const errors = require('./errors');

const PREFIX = '/api';
const PORT = 8080;
const DB_FILE = 'db/data.db';

async function main() {
    const app = express();
    const db = new DbService(DB_FILE);
    const issueRepository = new IssueRepository(db);
    const issues = issuesEndpoint(issueRepository, errors);

    app.use(bodyParser.json());
    app.use(PREFIX, issues);
    // eslint-disable-next-line
    app.use((err, req, res, next) => {
        if (err instanceof errors.GenericError) {
            res.status(err.code).send(err.defaultMessage);
            return;
        }
        console.error(err);
        res.status(500).send({ error: 'InternalServerError', message: 'Internal server error'});
    });

    app.listen(PORT, () => console.log(`Listen on ${PORT}`));
}

main();