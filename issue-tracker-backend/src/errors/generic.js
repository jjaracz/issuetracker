class GenericError extends Error {
    constructor({ code, message }) {
        super();
        this.code = code;
        this.defaultMessage = message;
    }
}

module.exports = GenericError;