const GenericError = require('./generic');

class ValidationError extends GenericError {
    constructor({code = 400, message =  'Invalid payload' } = {}) {
        super({code, message: { error: 'ValidationError', message }});
    }
}

class NotFoundError extends GenericError {
    constructor({code = 404, message =  'Can not find resource' } = {}) {
        super({code, message: { error: 'NotFoundError', message }});
    }
}

module.exports = {
    GenericError,
    ValidationError,
    NotFoundError
};
