class IssuesRepository {

    constructor(db) {
        this.db = db;
        this.db.runQuery(`
			CREATE TABLE IF NOT EXISTS issues (
				id INTEGER PRIMARY KEY AUTOINCREMENT,
				title TEXT NOT NULL,
				description TEXT NOT NULL,
				status TEXT NOT NULL DEFAULT 'open'
			)
		`);
    }

    getIssues() {
        return this.db.all('SELECT * FROM issues');
    }

    getIssueById(id) {
        return this.db.get('SELECT * FROM issues WHERE id = ?', [id]);
    }

    createIssue(issue) {
        return this.db.runQuery('INSERT INTO issues (title, description) VALUES (?, ?)', [issue.title, issue.description]);
    }

    updateStatus(issue) {
        return this.db.runQuery('UPDATE issues SET status = ? WHERE id = ?', [issue.status, issue.id]);
    }
}

module.exports = IssuesRepository;