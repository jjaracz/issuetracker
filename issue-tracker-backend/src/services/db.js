const { Database } = require('sqlite3').verbose();

class DbService {

    constructor(filename = 'db/data') {
        this._database = new Database(filename, (err) => {
            if (err) {
                console.error('Could not connect to database', err);
                return;
            }
            console.log('Connected to database');
        });
    }

    async runQuery(sql, params = []) {
        return new Promise((resolve, reject) => {
            this._database.run(sql, params, (err) => {
                if (err) {
                    console.error(err);
                    reject(err);
                    return;
                }
                resolve();
            });
        });
    }

    async all(sql) {
        return new Promise((resolve, reject) => {
            this._database.all(sql, (err, row) => {
                if (err) {
                    console.error(err);
                    reject(err);
                    return;
                }
                resolve(row);
            });
        });
    }

    async get(sql, params = []) {
        return new Promise((resolve, reject) => {
            this._database.get(sql, params, (err, result) => {
                if (err) {
                    console.log(err);
                    reject(err);

                    return;
                }
                resolve(result);
            });
        });
    }
}

module.exports = DbService;