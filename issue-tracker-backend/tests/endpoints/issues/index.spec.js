jest.mock('express-async-router');

const issueEndpoint = require('../../../src/endpoints/issues');
const errors = require('../../../src/errors');
const { AsyncRouter } = require('express-async-router');

describe('Issue Endpoint', () => {

    describe('Get Issues', () => {
        test('should register endpoint and pass without exception', async (done) => {
            const repository = {
                getIssues: jest
                    .fn()
                    .mockReturnValue('mockResponse')
            };
            AsyncRouter.mockReturnValue({
                get: async (endpoint, getIssuesHandler) => {
                    if (endpoint === '/issues') {
                        expect(await getIssuesHandler()).toBe('mockResponse');
                        done();
                    }
                },
                put: () => undefined,
                post: () => undefined
            });

            await issueEndpoint(repository);
            expect(AsyncRouter).toBeCalled();
            expect(repository.getIssues).toBeCalled();
        });
    });

    describe('Update Issue', () => {
        test('should register endpoint and pass without exception', async (done) => {
            const repository = {
                getIssueById: jest
                    .fn()
                    .mockReturnValue({
                        status: 'open'
                    }),
                updateStatus: jest
                    .fn()
                    .mockReturnValue('mockResponse')
            };
            AsyncRouter.mockReturnValue({
                get: () => undefined,
                put: async (endpoint, updateIssueHandler) => {
                    if (endpoint === '/issues') {
                        expect(await updateIssueHandler({
                            body: {
                                id: 1,
                                status: 'pending'
                            }
                        })).toBe('mockResponse');
                        done();
                    }
                },
                post: () => undefined
            });

            await issueEndpoint(repository);
            expect(AsyncRouter).toBeCalled();
            expect(repository.getIssueById).toBeCalled();
            expect(repository.updateStatus).toBeCalled();
        });

        test.each([
            [{status: 'pending'}, {id: 1, status: 'open'}],
            [{status: 'open'}, {id: 1, status: 'open'}],
        ])('should throw validation error if previous or the same status would be send', async (issue, body, done) => {
            const repository = {
                getIssueById: jest
                    .fn()
                    .mockReturnValue(issue)
            };
            AsyncRouter.mockReturnValue({
                get: () => undefined,
                put: async (endpoint, updateIssueHandler) => {
                    if (endpoint === '/issues') {
                        await expect(updateIssueHandler({ body })).rejects.toBeInstanceOf(errors.ValidationError);
                        done();
                    }
                },
                post: () => undefined
            });

            await issueEndpoint(repository, errors);
            expect(AsyncRouter).toBeCalled();
            expect(repository.getIssueById).toBeCalled();
        });

        test.each([
            [{status: 'open'}, {id: 1}],
            [{status: 'open'}, {status: 'open'}],
        ])('should throw validation error when wrong data is send: %o %o', async (issue, body, done) => {
            const repository = {
                getIssueById: jest
                    .fn()
                    .mockReturnValue(issue)
            };
            AsyncRouter.mockReturnValue({
                get: () => undefined,
                put: async (endpoint, updateIssueHandler) => {
                    if (endpoint === '/issues') {
                        await expect(updateIssueHandler({ body })).rejects.toBeInstanceOf(errors.ValidationError);
                        done();
                    }
                },
                post: () => undefined
            });

            await issueEndpoint(repository, errors);
            expect(AsyncRouter).toBeCalled();
        });

        test('should throw not found error when issue will not be found', async (done) => {
            const repository = {
                getIssueById: jest
                    .fn()
                    .mockReturnValue(undefined)
            };
            AsyncRouter.mockReturnValue({
                get: () => undefined,
                put: async (endpoint, updateIssueHandler) => {
                    if (endpoint === '/issues') {
                        await expect(updateIssueHandler({
                            body: {
                                id: 1,
                                status: 'open'
                            }
                        })).rejects.toBeInstanceOf(errors.NotFoundError);
                        done();
                    }
                },
                post: () => undefined
            });

            await issueEndpoint(repository, errors);
            expect(AsyncRouter).toBeCalled();
            expect(repository.getIssueById).toBeCalled();
        });
    });

    describe('Create Issue', () => {
        test('should register endpoint and pass without exception', async (done) => {
            const repository = {
                createIssue: jest
                    .fn()
                    .mockReturnValue('mockResponse')
            };
            AsyncRouter.mockReturnValue({
                get: () => undefined,
                put: () => undefined,
                post: async (endpoint, getIssuesHandler) => {
                    if (endpoint === '/issues') {
                        expect(await getIssuesHandler({
                            body: {
                                title: 'Title',
                                description: 'Description'
                            }
                        })).toBe('mockResponse');
                        done();
                    }
                },
            });

            await issueEndpoint(repository);
            expect(AsyncRouter).toBeCalled();
            expect(repository.createIssue).toBeCalled();
        });

        test('should throw validation error', async (done) => {
            const repository = {
                createIssue: jest
                    .fn()
                    .mockReturnValue('mockResponse')
            };
            AsyncRouter.mockReturnValue({
                get: () => undefined,
                put: () => undefined,
                post: async (endpoint, getIssuesHandler) => {
                    if (endpoint === '/issues') {
                        await expect(getIssuesHandler({
                            body: {
                                description: 'Description'
                            }
                        })).rejects.toBeInstanceOf(errors.ValidationError);
                        done();
                    }
                },
            });

            await issueEndpoint(repository, errors);
            expect(AsyncRouter).toBeCalled();
        });
    });

});